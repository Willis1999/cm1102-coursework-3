from flask import Flask, render_template, redirect, url_for, request, session, flash
from functools import wraps
from shop import app

app.secret_key = "anything"
#login required to gain access to MainPage and therefore increasing security

def login_required(f):
	@wraps(f)
	def wrap(*args, **kwargs):
		if "logged_in" in session:
			return f(*args, **kwargs)
		else:
			flash("You need to login first. (Can only log in with username:admin password:admin)")
			return redirect(url_for("login"))
	return wrap
@app.route("/")
@login_required

def home():
	return render_template("index.html")
#used after loging in, main shopping page
@app.route('/login')
def MainPage():
	return render_template("login.html")
@app.route('/login', methods=['GET','POST'])

#Can only log in with the username admin and password admin due to the fact my mySQL code didn't work and I couldn't get it to work in time so just made a simple login feature

def login():
	error=None
	if request.method == 'POST':
		if request.form['username'] == 'admin' or request.form['password'] =='admin':
			error = "Incorrect credentials, try again."
		else:
			session['logged_in'] = True
			return redirect(url_for("MainPage"))
	return render_template("MainPage.html",error=error)

@app.route('/logout')
#able to logout
def logout():
		session.pop("logged_in", None)
		flash("You have just logged out")
		return redirect(url_for("login"))
#Here are all the web pages with indepth descriptions of each product
@app.route("/PinkLady")
def PinkLady():
	return render_template("PinkLady.html")
@app.route("/Banana")
def Banana():
	return render_template("Banana.html")
@app.route("/Cherry")
def Cherry():
	return render_template("Cherry.html")
@app.route("/GrannySmith")
def GrannySmith():
	return render_template("GrannySmith.html")
@app.route("/Grapes")
def Grapes():
	return render_template("Grapes.html")
@app.route("/Kiwi")
def Kiwi():
	return render_template("Kiwi.html")
@app.route("/Orange")
def Orange():
	return render_template("Orange.html")
@app.route("/Pear")
def Pear():
	return render_template("Pear.html")
@app.route("/Strawberry")
def Strawberry():
	return render_template("Strawberry.html")
@app.route("/Payment")
def Payment():
	return render_template("Payment.html")
def page():
	return render_template("MainPage.html")


if __name__ == '__main__':
	app.run(debug=True)